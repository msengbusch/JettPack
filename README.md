## JettPack

JettPack is an experimental fork of [Pufferfish](https://github.com/pufferfish-gg/Pufferfish) with some experimental stuff I'm working on. Don't expect it to work well, if at all. This is also very experimental, and no support will be provided, use at your own risk.

Here's some of the stuff in this fork:
1. Reduced allocations
2. [alternate-current](https://github.com/SpaceWalkerRS/alternate-current) redstone algorithm
3. Tons of optimizations from [lithium](https://github.com/CaffeineMC/lithium-fabric)
4. And much more!

Jenkins: https://www.gardling.com/jenkins
Discord: https://discord.gg/U9eQ93d2pB