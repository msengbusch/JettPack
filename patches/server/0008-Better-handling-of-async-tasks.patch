From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: Simon Gardling <titaniumtown@gmail.com>
Date: Thu, 8 Jul 2021 17:00:21 -0400
Subject: [PATCH] Better handling of async tasks

Replaces niche/one time use threadpools in other areas by instead using `asyncExecutor` in `MCUtil` This patch also adds a system of flushing small async tasks (hence the variable name of `smallAsyncTasks`) in the `asyncExecutor` threadpool.

diff --git a/src/main/java/net/minecraft/server/MCUtil.java b/src/main/java/net/minecraft/server/MCUtil.java
index f99d189f461921d37581e2fc1382af60921b0660..e7ffd16d52bde49169f28f4e18aad5cdce1ac8da 100644
--- a/src/main/java/net/minecraft/server/MCUtil.java
+++ b/src/main/java/net/minecraft/server/MCUtil.java
@@ -53,11 +53,14 @@ import java.util.concurrent.atomic.AtomicBoolean;
 import java.util.function.BiConsumer;
 import java.util.function.Consumer;
 import java.util.function.Supplier;
+import java.util.concurrent.SynchronousQueue; // Jettpack
+import java.util.concurrent.ConcurrentLinkedQueue; // Jettpack
 
 public final class MCUtil {
+    public static final ConcurrentLinkedQueue smallAsyncTasks = new ConcurrentLinkedQueue(); // Jettpack
     public static final ThreadPoolExecutor asyncExecutor = new ThreadPoolExecutor(
-        0, 2, 60L, TimeUnit.SECONDS,
-        new LinkedBlockingQueue<>(),
+        4, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, // Jettpack
+        new SynchronousQueue<Runnable>(), // Jettpack
         new ThreadFactoryBuilder()
             .setNameFormat("Paper Async Task Handler Thread - %1$d")
             .setUncaughtExceptionHandler(new net.minecraft.DefaultUncaughtExceptionHandlerWithName(MinecraftServer.LOGGER))
@@ -74,6 +77,30 @@ public final class MCUtil {
 
     public static final long INVALID_CHUNK_KEY = getCoordinateKey(Integer.MAX_VALUE, Integer.MAX_VALUE);
 
+    // Jettpack start
+    public static void flushAsyncTasks() {
+        if (!smallAsyncTasks.isEmpty()) {
+            asyncExecutor.submit(() -> {
+            Runnable runnable;
+            while((runnable = (Runnable)smallAsyncTasks.poll()) != null) {
+                    runnable.run();
+                }
+            });
+        }
+    }
+
+    public static void flushAsyncTasksMidTick() {
+        if (smallAsyncTasks.size() <= 16) {
+            asyncExecutor.submit(() -> {
+                Runnable runnable;
+                while((runnable = (Runnable)smallAsyncTasks.poll()) != null) {
+                    runnable.run();
+                }
+
+            });
+        }
+    }
+    // Jettpack end
 
     public static Runnable once(Runnable run) {
         AtomicBoolean ran = new AtomicBoolean(false);
diff --git a/src/main/java/net/minecraft/server/MinecraftServer.java b/src/main/java/net/minecraft/server/MinecraftServer.java
index 3c29362ae4544d0ee9ccfd686bce8495479947de..cf8b882cef92602e4b61253e1f0d790eed9072be 100644
--- a/src/main/java/net/minecraft/server/MinecraftServer.java
+++ b/src/main/java/net/minecraft/server/MinecraftServer.java
@@ -372,6 +372,7 @@ public abstract class MinecraftServer extends ReentrantBlockableEventLoop<Runnab
             return;
         }
 
+        MCUtil.flushAsyncTasksMidTick();
         co.aikar.timings.MinecraftTimings.midTickChunkTasks.startTiming();
         try {
             for (;;) {
@@ -1098,6 +1099,7 @@ public abstract class MinecraftServer extends ReentrantBlockableEventLoop<Runnab
         LOGGER.info("Flushing Chunk IO");
         com.destroystokyo.paper.io.PaperFileIOThread.Holder.INSTANCE.close(true, true); // Paper
         LOGGER.info("Closing Thread Pool");
+        MCUtil.flushAsyncTasks();
         Util.shutdownExecutors(); // Paper
         LOGGER.info("Closing Server");
         try {
@@ -1521,6 +1523,7 @@ public abstract class MinecraftServer extends ReentrantBlockableEventLoop<Runnab
         io.papermc.paper.util.CachedLists.reset(); // Paper
         // Paper start - move executeAll() into full server tick timing
         try (co.aikar.timings.Timing ignored = MinecraftTimings.processTasksTimer.startTiming()) {
+            MCUtil.flushAsyncTasks();
             this.runAllTasks();
         }
         // Paper end
diff --git a/src/main/java/net/minecraft/server/network/ServerLoginPacketListenerImpl.java b/src/main/java/net/minecraft/server/network/ServerLoginPacketListenerImpl.java
index 33a29890435d6065a2cc4f8e8bf8209c01d5d114..acc0ff5a31c43e4fa1745727ec98bf712b28f0e5 100644
--- a/src/main/java/net/minecraft/server/network/ServerLoginPacketListenerImpl.java
+++ b/src/main/java/net/minecraft/server/network/ServerLoginPacketListenerImpl.java
@@ -46,6 +46,7 @@ import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
 import org.bukkit.event.player.PlayerPreLoginEvent;
 // CraftBukkit end
 import io.netty.buffer.Unpooled; // Paper
+import net.minecraft.server.MCUtil;
 
 public class ServerLoginPacketListenerImpl implements ServerLoginPacketListener {
 
@@ -126,6 +127,7 @@ public class ServerLoginPacketListenerImpl implements ServerLoginPacketListener
 
     }
 
+    /*
     // Paper start - Cache authenticator threads
     private static final AtomicInteger threadId = new AtomicInteger(0);
     private static final java.util.concurrent.ExecutorService authenticatorPool = java.util.concurrent.Executors.newCachedThreadPool(
@@ -138,6 +140,7 @@ public class ServerLoginPacketListenerImpl implements ServerLoginPacketListener
             }
     );
     // Paper end
+    */
     // Spigot start
     public void initUUID()
     {
@@ -248,7 +251,7 @@ public class ServerLoginPacketListenerImpl implements ServerLoginPacketListener
             // Paper end
             // Spigot start
             // Paper start - Cache authenticator threads
-            authenticatorPool.execute(new Runnable() {
+            MCUtil.asyncExecutor.execute(new Runnable() { // Jettpack
                 @Override
                 public void run() {
                     try {
@@ -292,7 +295,7 @@ public class ServerLoginPacketListenerImpl implements ServerLoginPacketListener
         }
 
         // Paper start - Cache authenticator threads
-        authenticatorPool.execute(new Runnable() {
+        MCUtil.asyncExecutor.execute(new Runnable() { // Jettpack
             public void run() {
                 GameProfile gameprofile = ServerLoginPacketListenerImpl.this.gameProfile;
 
@@ -426,7 +429,7 @@ public class ServerLoginPacketListenerImpl implements ServerLoginPacketListener
             this.gameProfile = com.destroystokyo.paper.proxy.VelocityProxy.createProfile(buf);
 
             // Proceed with login
-            authenticatorPool.execute(() -> {
+            MCUtil.asyncExecutor.execute(() -> { // Jettpack
                 try {
                     new LoginHandler().fireEvents();
                 } catch (Exception ex) {
diff --git a/src/main/java/org/bukkit/craftbukkit/scheduler/CraftAsyncScheduler.java b/src/main/java/org/bukkit/craftbukkit/scheduler/CraftAsyncScheduler.java
index 9c1aff17aabd062640e3f451a2ef8c50a7c62f10..5ab5f75eb8ed958cb885ec051d5c84e7c4f4b2db 100644
--- a/src/main/java/org/bukkit/craftbukkit/scheduler/CraftAsyncScheduler.java
+++ b/src/main/java/org/bukkit/craftbukkit/scheduler/CraftAsyncScheduler.java
@@ -32,23 +32,30 @@ import java.util.Iterator;
 import java.util.List;
 import java.util.concurrent.Executor;
 import java.util.concurrent.Executors;
+/*
 import java.util.concurrent.SynchronousQueue;
 import java.util.concurrent.ThreadPoolExecutor;
 import java.util.concurrent.TimeUnit;
+*/
+import net.minecraft.server.MCUtil; // JettPack
 
 public class CraftAsyncScheduler extends CraftScheduler {
 
+    /*
     private final ThreadPoolExecutor executor = new ThreadPoolExecutor(
             4, Integer.MAX_VALUE,30L, TimeUnit.SECONDS, new SynchronousQueue<>(),
             new ThreadFactoryBuilder().setNameFormat("Craft Scheduler Thread - %1$d").setUncaughtExceptionHandler(new net.minecraft.DefaultUncaughtExceptionHandlerWithName(net.minecraft.server.MinecraftServer.LOGGER)).build()); // Paper
+    */
     private final Executor management = Executors.newSingleThreadExecutor(new ThreadFactoryBuilder()
             .setNameFormat("Craft Async Scheduler Management Thread").setUncaughtExceptionHandler(new net.minecraft.DefaultUncaughtExceptionHandlerWithName(net.minecraft.server.MinecraftServer.LOGGER)).build()); // Paper
     private final List<CraftTask> temp = new ArrayList<>();
 
     CraftAsyncScheduler() {
         super(true);
+        /*
         executor.allowCoreThreadTimeOut(true);
         executor.prestartAllCoreThreads();
+        */
     }
 
     @Override
@@ -93,7 +100,7 @@ public class CraftAsyncScheduler extends CraftScheduler {
     private boolean executeTask(CraftTask task) {
         if (isValid(task)) {
             this.runners.put(task.getTaskId(), task);
-            this.executor.execute(new ServerSchedulerReportingWrapper(task));
+            MCUtil.asyncExecutor.execute(new ServerSchedulerReportingWrapper(task)); // Jettpack
             return true;
         }
         return false;
diff --git a/src/main/java/org/spigotmc/WatchdogThread.java b/src/main/java/org/spigotmc/WatchdogThread.java
index bee38307494188800886a1622fed229b88dbd8f1..a45c2c569e0ab7205b8c180c3e150f3d421f648a 100644
--- a/src/main/java/org/spigotmc/WatchdogThread.java
+++ b/src/main/java/org/spigotmc/WatchdogThread.java
@@ -141,6 +141,7 @@ public class WatchdogThread extends Thread
     {
         while ( !this.stopping )
         {
+            net.minecraft.server.MCUtil.flushAsyncTasks(); // Jettpack
             //
             // Paper start
             Logger log = Bukkit.getServer().getLogger();
